package com.example.automatedcontrolbiofloctank;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TankListActivity extends AppCompatActivity {


    DatabaseReference DataRef;
    private ArrayList<Tank> arrayList;
    private ArrayList<String> arrayList2;

    TankRecylerViewAdapter tankRecylerViewAdapter;
    RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_list);

        DataRef = FirebaseDatabase.getInstance().getReference();


        tankRecylerViewAdapter= new TankRecylerViewAdapter(this);
         recyclerView=findViewById(R.id.recyelerview);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,2);

         recyclerView.setLayoutManager(gridLayoutManager);



        try {
            DataRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {


                        Toast.makeText(TankListActivity.this, "" +snapshot.child("Temperature").child("value").getValue() , Toast.LENGTH_LONG).show();

                        /*projectName.setText( snapshot.child("ProjectName").getValue().toString());
                        projectDescription.setText(snapshot.child("ProjectDescription").getValue().toString());

                        teacherContact.setText(  snapshot.child("TeacherContactNo").getValue().toString());
                        teacherEmail.setText( snapshot.child("TeacherEmail").getValue().toString());*/
                    }
                    tankRecylerViewAdapter.SetTankList(arrayList2);
                    recyclerView.setAdapter(tankRecylerViewAdapter);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.d("Database Error!", "" + error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
}