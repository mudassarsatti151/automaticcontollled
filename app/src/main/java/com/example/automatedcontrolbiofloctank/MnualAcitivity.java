package com.example.automatedcontrolbiofloctank;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class MnualAcitivity extends AppCompatActivity  {


    RadioGroup radioGroup;
    RadioButton r1, r2, r3, r4;
    DatabaseReference DataRef;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mnual_acitivity);

        back=findViewById(R.id.imageView);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        DataRef = FirebaseDatabase.getInstance().getReference();

        r1 = findViewById(R.id.r1);
        r2 = findViewById(R.id.r2);
        r3 = findViewById(R.id.r3);
        r4 = findViewById(R.id.r4);

         r1.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 if(r1.isSelected()){
                     r1.setChecked(false);
                     r1.setSelected(false);
                     DataRef.child("b1").setValue("1").addOnSuccessListener(new OnSuccessListener<Void>() {
                         @Override
                         public void onSuccess(Void aVoid) {
                             Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                             // startActivity(new Intent(MainActivity.this, Home.class));

                         }
                     });
                 }else{
                     r1.setSelected(true);
                     r1.setChecked(true);
                     DataRef.child("b1").setValue("2").addOnSuccessListener(new OnSuccessListener<Void>() {
                         @Override
                         public void onSuccess(Void aVoid) {
                             Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                             // startActivity(new Intent(MainActivity.this, Home.class));

                         }
                     });
                 }
             }
         });

        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(r2.isSelected()){
                    r2.setChecked(false);
                    r2.setSelected(false);
                    DataRef.child("b2").setValue("3").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                            // startActivity(new Intent(MainActivity.this, Home.class));

                        }
                    });
                }else{
                    r2.setSelected(true);
                    r2.setChecked(true);
                    DataRef.child("b2").setValue("4").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                            // startActivity(new Intent(MainActivity.this, Home.class));

                        }
                    });
                }
            }
        });

        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(r3.isSelected()){
                    r3.setChecked(false);
                    r3.setSelected(false);
                    DataRef.child("b3").setValue("5").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                            // startActivity(new Intent(MainActivity.this, Home.class));

                        }
                    });
                }else{
                    r3.setSelected(true);
                    r3.setChecked(true);
                    DataRef.child("b3").setValue("6").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                            // startActivity(new Intent(MainActivity.this, Home.class));

                        }
                    });
                }
            }
        });

        r4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(r4.isSelected()){
                    r4.setChecked(false);
                    r4.setSelected(false);
                    DataRef.child("b4").setValue("7").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                            // startActivity(new Intent(MainActivity.this, Home.class));

                        }
                    });
                }else{
                    r4.setSelected(true);
                    r4.setChecked(true);
                    DataRef.child("b4").setValue("8").addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Data Successfuly Changed For Button 1 in DataBase", Toast.LENGTH_LONG).show();

                            // startActivity(new Intent(MainActivity.this, Home.class));

                        }
                    });
                }
            }
        });

    }


}