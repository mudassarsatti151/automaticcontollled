package com.example.automatedcontrolbiofloctank;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.UserDictionary;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AutoChangeService extends Service {

    DatabaseReference DataRef;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service Started", Toast.LENGTH_SHORT).show();
        DataRef = FirebaseDatabase.getInstance().getReference();
        try {
            DataRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {

                        String temprature = (String) snapshot.child("Temprature").child("value").getValue();
                        String waterLevel = (String) snapshot.child("waterlevel").child("value").getValue();
                        String phValue = (String) snapshot.child("PH").child("value").getValue();

                        String regEx = "[^0-9.]";
                        String w1 = waterLevel.replaceAll("[^a-zA-z]", "");
                        String phInt = phValue.replaceAll("[^0-9.]", "");


                        String temp= temprature.replaceAll(regEx, "");
                           double temp1 = Double.parseDouble(temp);
                            if(temp1<20){
                                DataRef.child("b4").setValue("7").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(getApplicationContext(), "Data updated  For Button4 in DataBase", Toast.LENGTH_LONG).show();

                                    }
                                });
                            }else if(temp1>36){
                                DataRef.child("b1").setValue("1").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                     //   Toast.makeText(getApplicationContext(), "Data updated  For Button4 in DataBase", Toast.LENGTH_LONG).show();

                                    }
                                });
                            }
                            if(!(w1.contains("medium")||w1.contains("high"))){
                                DataRef.child("b1").setValue("1").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                //       Toast.makeText(getApplicationContext(), "Data updated  For Button4 in DataBase", Toast.LENGTH_LONG).show();

                                    }
                                });
                            }
                            double ph= Double.parseDouble(phInt);
                            if(ph<6 && ph>9){
                                DataRef.child("b1").setValue("1").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                   //     Toast.makeText(getApplicationContext(), "Data updated  For Button4 in DataBase", Toast.LENGTH_LONG).show();

                                    }
                                });
                            }else {

                                DataRef.child("b1").setValue("2").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                   //     Toast.makeText(getApplicationContext(), "Data updated  For Button4 in DataBase", Toast.LENGTH_LONG).show();

                                    }
                                });
                                DataRef.child("b4").setValue("8").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                   //     Toast.makeText(getApplicationContext(), "Data updated  For Button4 in DataBase", Toast.LENGTH_LONG).show();

                                    }
                                });

                            }



                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.d("Database Error!", "" + error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }


}
