package com.example.automatedcontrolbiofloctank;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class TankRecylerViewAdapter extends RecyclerView.Adapter<TankRecylerViewAdapter.tankViewHolder> {


    Context mContext;
    private DatabaseReference DataRef;
    private ArrayList<String> projectArrayList;

    public TankRecylerViewAdapter(Context context) {
        this.mContext = context;
    }

    public void SetTankList(ArrayList<String> mProjectsArrayList){
             this.projectArrayList=mProjectsArrayList;
    }


    @NonNull
    @Override
    public tankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_cardview, parent, false);
        return new tankViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull tankViewHolder holder, int position) {
            holder.value.setText(projectArrayList.get(position));


    }

    @Override
    public int getItemCount() {
        return projectArrayList.size();
    }

    public class tankViewHolder extends RecyclerView.ViewHolder{

        private TextView value;
        public tankViewHolder(@NonNull View itemView) {
            super(itemView);
            value=itemView.findViewById(R.id.value);

        }
    }

}
