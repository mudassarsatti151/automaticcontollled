package com.example.automatedcontrolbiofloctank;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TankHomeActivity extends AppCompatActivity {


    Button tanks, profile, logout;

    FirebaseAuth firebaseAuth;

    DatabaseReference DataRef;
    private ArrayList<Tank> arrayList;
    private ArrayList<String> arrayList2;

    TankRecylerViewAdapter tankRecylerViewAdapter;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_home);

        arrayList = new ArrayList<Tank>();
        arrayList2 = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        tanks = findViewById(R.id.button);
        profile = findViewById(R.id.button2);
        logout = findViewById(R.id.logout);

        DataRef = FirebaseDatabase.getInstance().getReference();


        tanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    DataRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()) {

                                String temprature = (String) snapshot.child("Temprature").child("value").getValue();
                                String waterLevel = (String) snapshot.child("waterlevel").child("value").getValue();
                                String ph = (String) snapshot.child("PH").child("value").getValue();



                                Intent intent = new Intent(TankHomeActivity.this, TankDetailActvity.class);
                                intent.putExtra(TankDetailActvity.TEMPERATURE, temprature);
                                intent.putExtra(TankDetailActvity.WATERLEVEL, waterLevel);
                                intent.putExtra(TankDetailActvity.PH, ph);
                                startActivity(intent);


                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.d("Database Error!", "" + error);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TankHomeActivity.this, ProfileActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TankHomeActivity.this, "logut ", Toast.LENGTH_LONG).show();

                if(firebaseAuth.getCurrentUser()!=null){
                    firebaseAuth.signOut();
                    startActivity(new Intent(TankHomeActivity.this, MainActivity.class));
                }


            }
        });
    }

    @Override
    public void onBackPressed() {
        if(firebaseAuth.getCurrentUser()!=null){
            return;
        }else {
            super.onBackPressed();
        }
    }
}