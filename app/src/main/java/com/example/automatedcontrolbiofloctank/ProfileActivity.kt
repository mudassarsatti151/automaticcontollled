package com.example.automatedcontrolbiofloctank

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore


class ProfileActivity : AppCompatActivity() {

    private lateinit var name: TextView
    private lateinit var email: TextView
    private lateinit var password: TextView

    var firebaseAuth: FirebaseAuth? = null
    private lateinit var db:FirebaseFirestore
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        db   = FirebaseFirestore.getInstance()
        name = findViewById(R.id.textView18)
        email = findViewById(R.id.textView19)
        password = findViewById(R.id.textView22)

        firebaseAuth = FirebaseAuth.getInstance()
        db.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid)
                .get().addOnCompleteListener { task: Task<DocumentSnapshot?> ->
                    if (task.isSuccessful && task.result != null) {
                        val firstName = task.result!!.getString("f_Name")
                        val emai = task.result!!.getString("Email")
                        val userpassword = task.result!!.getString("UserPassword")

                        email.setText( emai)
                        password.setText(userpassword)
                        name.setText(firstName)
                        //other stuff
                    } else {
                        //deal with error
                    }
                }

    }
}