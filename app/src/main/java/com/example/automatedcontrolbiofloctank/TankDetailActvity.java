package com.example.automatedcontrolbiofloctank;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TankDetailActvity extends AppCompatActivity {

    public static final String TEMPERATURE = "temprature";
    public static final String WATERLEVEL = "waterlevel";
    public static final String PH = "ph";
    TextView waterleve, temrature, ph;
    FloatingActionButton floatingActionButton, floatingActionButton2;

    AppCompatButton manual;
    SwitchCompat auto;

    Button tanks, profile, logout;
    FirebaseAuth firebaseAuth;

    DatabaseReference DataRef;
    private ArrayList<Tank> arrayList;
    private ArrayList<String> arrayList2;

    TankRecylerViewAdapter tankRecylerViewAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_detail_actvity);

        waterleve = findViewById(R.id.textView14);
        temrature = findViewById(R.id.textView13);
        ph = findViewById(R.id.textView16);
        auto = findViewById(R.id.auto);
        manual = findViewById(R.id.manual);


        auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startService(new Intent(TankDetailActvity.this, AutoChangeService.class));
                      //  startForegroundService(new Intent(TankDetailActvity.this, AutoChangeService.class));
                    }else {
                        startService(new Intent(TankDetailActvity.this, AutoChangeService.class));
                    }
                } else {
                        stopService(new Intent(TankDetailActvity.this, AutoChangeService.class));
                }
            }
        });

        manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TankDetailActvity.this, MnualAcitivity.class);
                startActivity(intent);
               // startForegroundService(intent);


            }
        });


        arrayList = new ArrayList<Tank>();
        arrayList2 = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        tanks = findViewById(R.id.button);
        profile = findViewById(R.id.button2);
        logout = findViewById(R.id.logout);

        DataRef = FirebaseDatabase.getInstance().getReference();

        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.floating_button2);

        Intent intent = getIntent();
        String tempratue = intent.getStringExtra(TEMPERATURE);
        String waterlevel = intent.getStringExtra(WATERLEVEL);
        String PHValue = intent.getStringExtra(PH);


        String w1 = waterlevel.replaceAll("[^a-zA-z]", " ");

        if (w1.replaceAll("[brn^]", " ").contains("Empty")) {
            waterleve.setText("Empty");
        } else {
            waterleve.setText(w1.replaceAll("[brn^]", " "));
        }

        temrature.setText(tempratue.replaceAll("[^0-9.]", " "));
        ph.setText(PHValue.replaceAll("[^0-9.]", " "));


        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DataRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()) {

                                String temprature = (String) snapshot.child("Temprature").child("value").getValue();
                                String waterLevel = (String) snapshot.child("waterlevel").child("value").getValue();
                                String phValue = (String) snapshot.child("PH").child("value").getValue();

                                String regEx = "[^0-9.]";
                                String w1 = waterLevel.replaceAll("[^a-zA-z]", " ");

                                if (w1.replaceAll("[brn^]", " ").contains("Empty")) {
                                    waterleve.setText("Empty");
                                } else {
                                    waterleve.setText(w1.replaceAll("[brn^]", " "));
                                }

                                temrature.setText(temprature.replaceAll(regEx, " "));
                                ph.setText(phValue.replaceAll("[^0-9.]", ""));

                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.d("Database Error!", "" + error);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }
}