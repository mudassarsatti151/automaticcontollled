package com.example.automatedcontrolbiofloctank;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {


    private AppCompatButton register, login;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();
        register = findViewById(R.id.signup);
        login = findViewById(R.id.singin);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startService(new Intent(MainActivity.this, AutoChangeService.class));
        } else {
            startService(new Intent(MainActivity.this, AutoChangeService.class));
        }
        if (firebaseAuth.getCurrentUser() != null) {
            startActivity(new Intent(this, TankHomeActivity.class));
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivityTwo.class);
                startActivity(intent);

            }
        });
    }
}